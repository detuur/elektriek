#include "Interface.h"


int adc_key_val[5] = { 50, 200, 400, 600, 800 };
int NUM_KEYS = 5;

int get_key(unsigned int input)
{
	int k;
	for (k = 0; k < NUM_KEYS; k++)
	{
		if (input < adc_key_val[k])
		{
			return k;
		}
	}
	if (k >= NUM_KEYS)k = -1;  // No valid key pressed
	return k;
}


// returns current room temperature
int get_temp()
{
	analogRead(PIN_Temp);
}

// returns whether analog �C tells us to run the fan
bool get_fan()
{
	return digitalRead(PIN_FanController);
}

// returns whether the fan output is high
bool isRunning_fan()
{
	return digitalRead(PIN_Fan);
}

// sets the fan output high or low
void set_fan(bool new_setting)
{
	digitalWrite(PIN_Fan, new_setting ? HIGH : LOW);
}

// returns whether analog �C tells us to run the heater
bool get_heat()
{
	return digitalRead(PIN_HeatController);
}

// returns whether heater output is high
bool isRunning_heat()
{
	return digitalRead(PIN_Heat);
}

// sets the heater output high or low
void set_heat(bool new_setting)
{
	digitalWrite(PIN_Heat, new_setting ? HIGH : LOW);
}