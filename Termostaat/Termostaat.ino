#include <LiquidCrystal.h>
#include "Interface.h"
#include "Header.h"
#include <arduino.h>
// lcd config
LiquidCrystal lcd(8, 13, 9, 4, 5, 6, 7);
int adc_key_in;
int key = -1;
int oldkey = -1;
int looping_timer = 0;

// Arrays for custom characters
/// degrees symbol �
uint8_t degrees[8] = {
	B00110,
	B01001,
	B01001,
	B00110,
	B00000,
	B00000,
	B00000,
};
/// double arrow ?
uint8_t arrow[8] = {
	B00100,
	B01110,
	B10101,
	B00100,
	B10101,
	B01110,
	B00100,
};

// Keep track of display parameters
Mode mode = Auto;
Screen screen = Status;
char menu_entry = 0;
int heat_setting = 20;
int heat_margin = 5;

// Pin Numbers


// Function Definitions
char* boolToString(bool in);
void drawStatusScreen(Button but);
void drawMenu(Button but);
void drawSubMenu(int index);
void executeLogic(Mode mode);
Button get_input();


void setup()
{
	//Serial.begin(9600);
	pinMode(PIN_Fan, OUTPUT);
	pinMode(PIN_Heat, OUTPUT);

	lcd.createChar(0, degrees);
	lcd.createChar(1, arrow);
	lcd.begin(16, 2);
	/*
	lcd.clear();
	lcd.setCursor(0, 0);
	lcd.print("     helle! ");
	lcd.print("      welcome!");
	lcd.setCursor(0, 1);
	lcd.print("   LinkSprite");
	lcd.print("    LCD Shield");
	delay(1000);

	lcd.setCursor(0, 0);
	for (char k = 0; k<26; k++)
	{
		lcd.scrollDisplayLeft();
		delay(400);
	}
	*/
	lcd.clear();
	lcd.setCursor(0, 0);
	lcd.print("ayy");
	lcd.setCursor(0, 1);
	lcd.print("lmao");
	delay(200);
}

void loop()
{
	Button but = get_input();
	lcd.clear();
	switch(screen)
	{
	case Status:
		drawStatusScreen(but);
		delay(250);
		break;
	case Menu:
		drawMenu(but);
		delay(25);
		break;
	}
	executeLogic(mode);
}

void drawStatusScreen(Button but)
{
	lcd.setCursor(0, 0);
	lcd.print("Mode:");
	char* st;
	switch (mode)
	{
	case Auto: st = "Auto"; break;
	case Man: st = "Man"; break;
	case Off: st = "Off"; break;
	default: st = "N/A";
	}
	lcd.print(st);

	lcd.setCursor(10, 0);
	lcd.print("T:");
	lcd.print(get_temp());
	lcd.write(uint8_t(0));
	lcd.print("C");

	lcd.setCursor(0, 1);
	lcd.print("Fan:");
	lcd.print(boolToString(isRunning_fan()));

	lcd.setCursor(8, 1);
	if (looping_timer > 4 && mode == Man)
	{
		lcd.print("Tgt:");
		lcd.print(heat_setting);
		lcd.write(uint8_t(0));
		lcd.print("C");
	}
	else
	{
		lcd.print("Heat:");
		lcd.print(boolToString(isRunning_heat()));
	}
	looping_timer = (looping_timer + 1) % 10;

	if (but != None)
	{
		screen = Menu;
		menu_entry = 1;
	}
	//delay(250); //moved to loop()
}

void drawMenu(Button but)
{
	int max_entries;
	switch (mode)
	{
	case Auto:
	case Off: max_entries = 2; break;
	case Man: max_entries = 4; break;
	default: max_entries = 1;
	}
	menu_entry = menu_entry % max_entries;
	lcd.setCursor(0, 0);
	lcd.print("MENU:");
	lcd.setCursor(14, 0);
	lcd.print("/");
	lcd.print(max_entries);
	lcd.setCursor(13, 0);
	if (mode == Man && menu_entry == 2)
	{
		lcd.print(2);
		drawSubMenu(1);
		if (but == None)
			return;
		if (but == Right && heat_setting < 50)
			++heat_setting;
		if (but == Left && heat_setting > 15)
			--heat_setting;
		set_heat(heat_setting);
	}
	else if (mode == Man && menu_entry == 3)
	{
		lcd.print(3);
		drawSubMenu(5);
		if (but == None)
			return;
		if (but == Right && heat_margin < 9)
			++heat_margin;
		if (but == Left && heat_margin > 1)
			--heat_margin;
	}
	else if ((mode == Man || mode == Off || mode == Auto) && menu_entry == 1)
	{
		lcd.print(1);
		drawSubMenu(3);
		if (but == None)
			return;
		if (but == Right)
			mode = Mode((mode + 1) % 3);
		if (but == Left)
			mode = Mode((mode + 2) % 3);
	}
	else
	{
		lcd.print(max_entries);
		drawSubMenu(4);
		if (but == Select)
		{
			screen = Status;
		}
	}
	if (but == Up)
		menu_entry += max_entries - 1;
	if (but == Down)
		++menu_entry;
}

void drawSubMenu(int index)
{
	lcd.setCursor(0, 1);
	lcd.write(uint8_t(1));
	if (index == 1)
	{
		lcd.print("Set Heat <");
		lcd.print(heat_setting);
		lcd.setCursor(13, 1);
		lcd.write(uint8_t(0));
		lcd.print("C>");
		return;
	}
	/*if (index == 2)
	{
	lcd.print("Set Fan  <");
	lcd.print(boolToString(fan_setting));
	lcd.setCursor(14, 1);
	lcd.print(">");
	return;
	}*/
	if (index == 3)
	{
		lcd.print("Set Mode");
		lcd.setCursor(10, 1);
		lcd.print("<");
		char* st;
		switch (mode)
		{
		case Auto: st = "Auto"; break;
		case Man: st = "Man"; break;
		case Off: st = "Off"; break;
		default: st = "N/A";
		}
		lcd.print(st);
		lcd.setCursor(15, 1);
		lcd.print(">");
		return;
	}
	if (index == 4)
	{
		lcd.setCursor(5, 1);
		lcd.print(">Exit<");
		return;
	}
	if (index == 5)
	{
		lcd.print("Tolerance <");
		lcd.print(heat_margin);
		lcd.write(uint8_t(0));
		lcd.print("C>");
	}
}

void executeLogic(Mode mode)
{
	switch (mode)
	{
	case Off:
		set_fan(false);
		set_heat(false);
		return;
	case Man:
		if (get_temp() == heat_setting)
		{
			set_fan(false);
			set_heat(false);
			return;
		}
		if (get_temp() + heat_margin < heat_setting)
		{
			set_heat(true);
			set_fan(false);
			return;
		}
		if (get_temp() - heat_margin > heat_setting)
		{
			set_fan(true);
			set_heat(false);
		}
		return;
	case Auto:
		set_fan((get_fan()));
		set_heat(get_heat());
		return;
	}
}

Button get_input()
{
	adc_key_in = analogRead(0);    // read the value from the sensor 
	key = get_key(adc_key_in);  // convert into key press
	if (key != oldkey)   // if keypress is detected
	{
		delay(50);  // wait for debounce time
		adc_key_in = analogRead(0);    // read the value from the sensor 
		key = get_key(adc_key_in);    // convert into key press
		if (key != oldkey)
		{
			lcd.setCursor(0, 1);
			oldkey = key;
			if (key >= 0)
			{
				Serial.print("Button: ");
				Serial.println(key + 1);
				return (Button)(key + 1);
			}
		}
	}
	return None;
}

char* boolToString(bool in)
{
	if (in)
		return "On";
	return "Off";
}