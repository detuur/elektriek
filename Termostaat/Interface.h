// Interface.h

#ifndef _INTERFACE_h
#define _INTERFACE_h
#include "Header.h"


// function definitions
int get_temp();
bool get_fan();
bool isRunning_fan();
void set_fan(bool new_setting);
bool get_heat();
bool isRunning_heat();
void set_heat(bool new_setting);
int get_key(unsigned int input);

#endif


