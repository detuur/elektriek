#ifndef _HEADER_h
#define _HEADER_h
#include <Arduino.h>

// pin configuration
#define PIN_FanController A1
#define PIN_Fan A2
#define  PIN_HeatController A3
#define  PIN_Heat A4
#define  PIN_Temp A5

// enums
enum Button { None, Right, Up, Down, Left, Select };
enum Screen { Status, Menu };
enum Mode { Auto, Man, Off };
#endif